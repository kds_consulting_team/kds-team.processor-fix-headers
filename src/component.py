import glob
import json
import logging
import os
import shutil
from csv import DictReader, DictWriter
from dataclasses import dataclass, field
from pathlib import Path

from keboola.component import ComponentBase, UserException

# global constants'


# configuration variables
KEY_FIXED_HEADER = 'fixed_header'
KEY_IGNORE_CASING = 'ignore_casing'
# #### Keep for debug
KEY_DEBUG = 'debug'
MANDATORY_PARS = [KEY_FIXED_HEADER]


@dataclass
class TableDef:
    path: str
    file_name: str
    is_sliced: bool
    manifest: dict = field(default_factory=dict)


class Component(ComponentBase):

    def __init__(self):
        ComponentBase.__init__(self)
        self.validate_configuration_parameters(MANDATORY_PARS)

    def run(self):
        """
        Main execution code
        """
        params = self.configuration.parameters
        tables = self.get_tables_def()
        fixed_header = params.get(KEY_FIXED_HEADER)
        ignore_casing = params.get(KEY_IGNORE_CASING)
        if not fixed_header:
            raise ValueError("No header is specified, please specify the expected fixed header")
        for t in tables:
            if len(tables) == 1 and not t.is_sliced and not fixed_header:
                logging.info('Just a single file on the input, nothing to change')
                self._copy_table_to_out(t)
                self._copy_manifest_to_out(t)
                break

            self.fix_header(t, params.get(KEY_FIXED_HEADER), ignore_casing)

        # move files
        if os.path.exists(self.files_in_path):
            shutil.copytree(self.files_in_path, self.files_out_path, dirs_exist_ok=True)

    def get_tables_def(self):
        table_files = [f for f in glob.glob(self.tables_in_path + "/**", recursive=False) if
                       not f.endswith('.manifest')]
        table_defs = list()
        for t in table_files:
            is_sliced = False
            manifest = dict()
            p = Path(t)
            if Path(t + '.manifest').exists():
                manifest = json.load(open(t + '.manifest'))

            if p.is_dir():
                is_sliced = True

            table_defs.append(TableDef(path=t, file_name=p.name,
                                       is_sliced=is_sliced, manifest=manifest))
        return table_defs

    def fix_header(self, t, fixed_header, ignore_casing):
        header = self.get_header(t)
        if not fixed_header:
            fixed_header = []

        if header == fixed_header:
            logging.info(f'File {t.file_name} contains the same header {fixed_header}, keeping as is')
            self._copy_table_to_out(t)
            self._copy_manifest_to_out(t)
            return

        delimiter = t.manifest.get('delimiter', ',')
        enclosure = t.manifest.get('enclosure', '"')

        if t.is_sliced:
            for tb_path in glob.glob(t.path + "/**", recursive=False):
                self._normalize_header(tb_path, os.path.join(Path(tb_path).parent.name, Path(tb_path).name),
                                       fixed_header, delimiter, enclosure, ignore_casing)
            self.replace_header_in_manifest_and_move(t.path, t.manifest, fixed_header)
        else:
            self._normalize_header(t.path, t.file_name, fixed_header, delimiter, enclosure, ignore_casing)
            self._copy_manifest_to_out(t)

    def _normalize_header(self, tbl_path, tbl_filename, fixed_header, delimiter=',',
                          enclosure='"', ignore_casing=False):
        new_file = Path(self.tables_out_path + '/' + tbl_filename)
        new_file.parent.mkdir(parents=True, exist_ok=True)
        with open(tbl_path) as input, open(new_file, 'wt+') as out_f:
            writer = DictWriter(
                out_f, fieldnames=fixed_header, extrasaction='ignore')
            reader = DictReader(input, lineterminator='\n',
                                delimiter=delimiter, quotechar=enclosure)
            writer.writeheader()

            reader_headers = {}
            for header in reader.fieldnames:
                reader_headers[header.lower()] = header

            for line in reader:
                if ignore_casing:
                    tmp = {}
                    for h in fixed_header:
                        value = line.get(reader_headers[h.lower()]) if h.lower() in reader_headers else ''
                        tmp[h] = value
                    writer.writerow(tmp)
                else:
                    row = line
                    writer.writerow(row)

    def _copy_manifest_to_out(self, t):
        if t.manifest:
            new_path = os.path.join(
                self.tables_out_path, Path(t.path).name + '.manifest')
            shutil.copy(t.path + '.manifest', new_path)

    @staticmethod
    def get_header(t: TableDef):
        if t.is_sliced and t.manifest:
            header = t.manifest['columns']
        elif t.is_sliced:
            header = []
        else:
            with open(t.path) as input:
                delimiter = t.manifest.get('delimiter', ',')
                enclosure = t.manifest.get('enclosure', '"')
                reader = DictReader(input, lineterminator='\n',
                                    delimiter=delimiter, quotechar=enclosure)
                header = reader.fieldnames

        return header

    def replace_header_in_manifest_and_move(self, file_path, manifest, new_header):
        manifest['columns'] = new_header
        with open(os.path.join(self.tables_out_path, Path(file_path).name + '.manifest'), 'w+') as out_f:
            json.dump(manifest, out_f)

    def _copy_table_to_out(self, t):
        if Path(t.path).is_dir():
            shutil.copytree(t.path, Path(
                self.tables_out_path).joinpath(t.file_name))
        else:
            shutil.copy(t.path, Path(
                self.tables_out_path).joinpath(t.file_name))


"""
        Main entrypoint
"""
if __name__ == "__main__":
    try:
        comp = Component()
        # this triggers the run method by default and is controlled by the configuration.action parameter
        comp.execute_action()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
