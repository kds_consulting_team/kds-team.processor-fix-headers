# Header fixing processor

Takes all tables in `in/tables/` or in subfolders and fixes the header to the predefined set of columns 
(adds emtpy values if column is not present).


**Table of contents:**  
  
[TOC]

## Functional notes

Provide a list of expected columns and the processor will make sure that all these columns are present in the file. If 
some column is not found, it will be added with empty values.

The files will be processed **only if the header is different** than the expected one.



## Configuration

- **fixed_header** - list of columns that will be always present in the output file
- **ignore_casing** - will ignore column cases when matching columns

### Sample configuration

```json
{
        "definition": {
          "component": "kds-team.processor-fix-headers"
        },
        "parameters": {
          "fixed_header": [
            "InvoiceID",
            "PreviousInvoiceId",
            "BillingAccountId",
            "BillingAccountName",
            "BillingProfileId",
            "BillingProfileName",
            "InvoiceSectionId"
          ],
        "ignore_casing": false
        }
      }
```
 
# Development
 
This example contains runnable container with simple unittest. For local testing it is useful to include `data` folder in the root
and use docker-compose commands to run the container or execute tests. 

If required, change local data folder (the `CUSTOM_FOLDER` placeholder) path to your custom path:
```yaml
    volumes:
      - ./:/code
      - ./CUSTOM_FOLDER:/data
```

Clone this repository, init the workspace and run the component with following command:

```
git clone https://bitbucket.org:kds_consulting_team/kds-team.processor-fix-headers.git my-new-component
cd my-new-component
docker-compose build
docker-compose run --rm dev
```

Run the test suite and lint check using this command:

```
docker-compose run --rm test
```

# Testing

The preset pipeline scripts contain sections allowing pushing testing image into the ECR repository and automatic 
testing in a dedicated project. These sections are by default commented out. 

# Integration

For information about deployment and integration with KBC, please refer to the [deployment section of developers documentation](https://developers.keboola.com/extend/component/deployment/) 